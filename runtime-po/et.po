# This file is distributed under the same license as the bison package.
# Estonian translations for bison.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Toomas Soome <Toomas.Soome@microlink.ee>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: bison-runtime 2.6.90\n"
"Report-Msgid-Bugs-To: bug-bison@gnu.org\n"
"POT-Creation-Date: 2019-12-11 07:43+0100\n"
"PO-Revision-Date: 2013-04-04 00:39+0300\n"
"Last-Translator: Toomas Soome <Toomas.Soome@microlink.ee>\n"
"Language-Team: Estonian <linux-ee@lists.eenet.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/skeletons/glr.c:884 data/skeletons/yacc.c:669
msgid "syntax error: cannot back up"
msgstr "süntaksi viga: ei saa toetada"

#: data/skeletons/glr.c:1793
msgid "syntax is ambiguous"
msgstr "süntaks on segane"

#: data/skeletons/glr.c:2079 data/skeletons/glr.c:2164
#: data/skeletons/glr.c:2208 data/skeletons/glr.c:2446
#: data/skeletons/lalr1.cc:1385 data/skeletons/lalr1.cc:1406
#: data/skeletons/yacc.c:1250 data/skeletons/yacc.c:1762
#: data/skeletons/yacc.c:1768
msgid "syntax error"
msgstr "süntaksi viga"

#: data/skeletons/glr.c:2165 data/skeletons/lalr1.cc:1386
#: data/skeletons/yacc.c:1251
#, c-format
msgid "syntax error, unexpected %s"
msgstr "süntaksi viga, ootamatu %s"

#: data/skeletons/glr.c:2166 data/skeletons/lalr1.cc:1387
#: data/skeletons/yacc.c:1252
#, c-format
msgid "syntax error, unexpected %s, expecting %s"
msgstr "süntaksi viga, ootamatu %s, ootasin %s"

#: data/skeletons/glr.c:2167 data/skeletons/lalr1.cc:1388
#: data/skeletons/yacc.c:1253
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s"
msgstr "süntaksi viga, ootamatu %s, ootasin %s või %s"

#: data/skeletons/glr.c:2168 data/skeletons/lalr1.cc:1389
#: data/skeletons/yacc.c:1254
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s or %s"
msgstr "süntaksi viga, ootamatu %s, ootasin %s või %s või %s"

#: data/skeletons/glr.c:2169 data/skeletons/lalr1.cc:1390
#: data/skeletons/yacc.c:1255
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s or %s or %s"
msgstr "süntaksi viga, ootamatu %s, ootasin %s või %s või %s või %s"

#: data/skeletons/glr.c:2505 data/skeletons/yacc.c:1335
#: data/skeletons/yacc.c:1337 data/skeletons/yacc.c:1526
#: data/skeletons/yacc.c:1918
msgid "memory exhausted"
msgstr "mälu on otsas"
